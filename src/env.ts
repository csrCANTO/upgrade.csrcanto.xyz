/* mainnet */

const env = {
  CHAIN: {
    ID: 7700,
    NAME: "Canto",
    RPC_URL: ["https://canto.gravitychain.io/"],
    EXPLORER_URL: ["https://tuber.build/"],
  },
  CONTRACTS: {
    CSRCANTO: "0xe73191C7D3a47E45780c76cB82AE091815F4C8F9"
  },
} as const;

/* testnet */

// const env = {
//   CHAIN: {
//     ID: 7701,
//     NAME: "Canto Testnet",
//     RPC_URL: ["https://canto-testnet.plexnode.wtf"],
//     EXPLORER_URL: ["https://testnet.tuber.build"],
//   },
//   CONTRACTS: {
//     CSRCANTO: "0xD70222d458E8e80Cf270Ac39D3be8699E503CE34"
//   },
// } as const;

export default env;
