import './App.css';

import { 
  createTheme,
  NextUIProvider,
} from '@nextui-org/react';
import { WalletProvider } from "./context/WalletContext";
import Main from './components/Main';

const theme = createTheme({
  type: 'dark',
  theme: {
    colors: {
      warning: "#eac85a",
      warningShadow: "#eac85a",
      secondary: "#000",
    }
  }
});

function App() {
  return (
    <WalletProvider>
      <NextUIProvider theme={theme}>
        <Main />
      </NextUIProvider>
    </WalletProvider>
  );
}

export default App;
