import { 
  Card,
  Container,
  Spacer,
  Text,
  Image,
  Grid,
  Button
} from '@nextui-org/react';

import UnwrapButton from './UnwrapButton';
import { useState } from 'react';

function Main() {
  const [isHover, setIsHover] = useState(false);

  const handleMouseEnter = () => { setIsHover(true); };
  const handleMouseLeave = () => { setIsHover(false); };

  const v2ButtonStyle = { backgroundColor: isHover ? '#eac85a' : "#000" }

  return (
    <Container css={{textAlign: 'center'}}>
      <Spacer y={2}/>
      <Card>
        <Card.Header>
          <Grid.Container justify='center' alignItems='center'>
            <Grid>
              <Image width={100} autoResize src='icon-csrcanto.png' alt='csrcanto' />
            </Grid>
            <Grid>
              <Text h1 css={{margin: 'auto'}}>
                $csrCANTO v2 is live
              </Text>
            </Grid>
          </Grid.Container>
        </Card.Header>
        <Card.Body>
          <Text h1 css={{margin: 'auto'}}>
            The next chapter begins now.
          </Text>
          <Spacer y={2}/>
          <Text css={{textAlign: 'center'}}>
            We have been busy tinkering, experimenting, and building to bring you the new and improved $csrCANTO v2.
          </Text>
          <Text css={{textAlign: 'center'}}>
            It is leaner, meaner, and more efficient. In terms of UX, you will not notice any difference however there are big changes to how $csrCANTO can be used by other protocols and ecosystem partners. 
          </Text>
          <Spacer y={2}/>
          <Text h2 css={{textAlign: 'center'}}>
            Upgrade your $csrCANTO to v2 now!
          </Text>
          <Text css={{textAlign: 'center'}}>
            All you need to do is to unwrap your current $csrCANTO and wrap it back into $csrCANTO v2.
          </Text>
          <Spacer y={2}/>
          <UnwrapButton />
          <Spacer y={2}/>
          <Button
            color="secondary"
            style={v2ButtonStyle}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            onClick={() => window.open('https://csrcanto.xyz', '_blank')}
          >
            Take me to $csrCANTO v2!
          </Button>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default Main;
