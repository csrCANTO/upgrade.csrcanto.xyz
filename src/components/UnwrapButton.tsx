import { useState } from "react";
import { ethers } from "ethers";
import { Button, Grid, Loading } from "@nextui-org/react";
import { ArrowUpSquare } from "react-iconly";

import { useWalletContext } from "../context/WalletContext";

import env from "../env";
import contract from "../contracts/CsrCanto.json";

function UnwrapButton() {
  const [loading, setLoading] = useState<boolean>(false);
  const [deactivated, setDeactivated] = useState<boolean>(false);

  const { 
    connectWallet,
    checkAndSwitchToCanto,
    signer,
    address
  } = useWalletContext();
  
  const csrCanto = new ethers.Contract(env.CONTRACTS.CSRCANTO, contract.abi, signer);

  const onConnect = async (): Promise<void> => {
    await checkAndSwitchToCanto();
    await connectWallet();
  };

  const onUnwrapAll = async (): Promise<void> => {
    setLoading(true);
    await unwrapAll();
    setLoading(false);
  };

  const unwrapAll = async (): Promise<void> => {
    try {
      const balance = await csrCanto.balanceOf(address);
      console.log(balance);
      if(Number(balance) === 0) {
        console.log("Nothing to unwrap");
        setDeactivated(true);
      }
      else {
        await csrCanto.withdraw(balance);
        setDeactivated(true);
      }
      setLoading(false);
    }
    catch(err) {
      alert(err);
    }
  };

  return (
    <Grid.Container justify="center" gap={1} >
      <Grid>
        <Button
          onPress={onConnect}
          shadow
          size="xl"
          disabled={!!address}
        >
          {!address ? "Connect" : "connected"}
        </Button>
        </Grid>
      <Grid>
        <Button
          css={{ width: "100%" }}
          shadow
          size="xl"
          color={deactivated ? "success" : "warning"}
          icon={<ArrowUpSquare set="bold"/>}
          onPress={onUnwrapAll}
          disabled={!address}
        >
          {loading
          ? <Loading type="points-opacity" />
          : deactivated
          ? "Nothing to unwrap"
          : "Unwrap all"
          }
        </Button>
      </Grid>
    </Grid.Container>
  )
}

export default UnwrapButton;