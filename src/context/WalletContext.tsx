import { ContractTransaction, ethers } from "ethers";
import { FC, ReactElement, createContext, useCallback, useContext, useEffect, useState } from "react";
import env from "../env";

type WalletContextType = {
  signer: ethers.Signer | null;
  provider: ethers.BrowserProvider | null;
  address: string | null;
  connectWallet: () => Promise<void>;
  checkAndSwitchToCanto: () => Promise<void>;
  sendTransaction: (
    populatedTx: ContractTransaction,
    inProgressMsg: string,
    successMsg: string
  ) => Promise<string | null>;
  isCorrectNetwork: boolean | null;
};

export const WalletContext = createContext<WalletContextType | undefined>(
  undefined
);

export const WalletProvider: FC<{ children: ReactElement }> = ({
  children,
}) => {
  const [provider, setProvider] = useState<ethers.BrowserProvider | null>(null);
  const [signer, setSigner] = useState<ethers.Signer | null>(null);
  const [address, setAddress] = useState<string>("");
  const [isCorrectNetwork, setIsCorrectNetwork] = useState<boolean | null>(
    null
  );

  useEffect(() => {
    if (window.ethereum) {
      const newProvider = new ethers.BrowserProvider(window.ethereum);
      setProvider(newProvider);
    }

    const handleChainChanged = (newChainId: string) => {
      setIsCorrectNetwork(parseInt(newChainId, 16) === env.CHAIN.ID);
    };

    const handleAccountsChanged = (newAccounts: string[]) => {
      setAddress(newAccounts[0]);
    };

    if (window.ethereum) {
      window.ethereum.on("chainChanged", handleChainChanged);
      window.ethereum.on("accountsChanged", handleAccountsChanged);
    }

    return () => {
      // Clean up the event listener on component unmount
      if (window.ethereum.removeListener) {
        window.ethereum.removeListener("chainChanged", handleChainChanged);
        window.ethereum.removeListener(
          "accountsChanged",
          handleAccountsChanged
        );
      }
    };
  }, []);

  const connectWallet = async (): Promise<void> => {
    if (!window.ethereum) {
      alert(
        "Please install a web3-enabled browser like MetaMask to use this feature."
      );
      return;
    }

    try {
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      setAddress(accounts[0]);
      const newProvider = new ethers.BrowserProvider(window.ethereum);
      setProvider(newProvider);
      const newSigner = await newProvider.getSigner();
      setSigner(newSigner);
    } catch (error) {
      console.error("Error connecting wallet:", error);
    }
  };

  const checkAndSwitchToCanto = async (): Promise<void> => {
    if (!window.ethereum) {
      alert("Please install MetaMask or another web3-enabled browser.");
      return;
    }

    const currentChainId = parseInt(window.ethereum.chainId, 10);
    if (currentChainId !== env.CHAIN.ID) {
      try {
        await window.ethereum.request({
          method: "wallet_switchEthereumChain",
          params: [{ chainId: `0x${env.CHAIN.ID.toString(16)}` }],
        });
      } catch (switchError) {
        if ((switchError as any).code === 4902) {
          try {
            await window.ethereum.request({
              method: "wallet_addEthereumChain",
              params: [
                {
                  chainId: `0x${env.CHAIN.ID.toString(16)}`,
                  chainName: env.CHAIN.NAME,
                  nativeCurrency: {
                    name: "Canto",
                    symbol: "CANTO",
                    decimals: 18,
                  },
                  rpcUrls: env.CHAIN.RPC_URL,
                  blockExplorerUrls: env.CHAIN.EXPLORER_URL,
                },
              ],
            });
          } catch (addError) {
            console.error("Failed to add Canto Testnet:", addError);
          }
        } else {
          console.error("Failed to switch to Canto Testnet:", switchError);
        }
      }
    }
  };

  const sendTransaction = useCallback(
    async (
      populatedTx: ContractTransaction,
      inProgressMsg: string,
      successMsg: string
    ) => {
      await checkAndSwitchToCanto();
      if (!signer) {
        // In theory this should never happen, but just in case
        // setStatus({
        //   msg: "Please connect your wallet first.",
        //   severity: "error",
        // });
        return null;
      }
      const tx = await signer.sendTransaction(populatedTx);
      // const explorerLink = `${env.CHAIN.EXPLORER_URL[0]}/tx/${tx.hash}`;
      // setStatus({
      //   msg: inProgressMsg,
      //   severity: "info",
      //   link: explorerLink,
      //   linkText: "View on Tuber",
      //   duration: null,
      // });
      await tx.wait();
      // setStatus({
      //   msg: successMsg,
      //   severity: "success",
      //   link: explorerLink,
      //   linkText: "View on Tuber",
      // });
      return tx.hash;
    },
    [signer]
  );

  return (
    <WalletContext.Provider
      value={{
        signer,
        provider,
        address,
        connectWallet,
        checkAndSwitchToCanto,
        isCorrectNetwork,
        sendTransaction
      }}
    >
      {children}
    </WalletContext.Provider>
  );
};

export const useWalletContext = (): WalletContextType => {
  const context = useContext(WalletContext);
  if (context === undefined) {
    throw new Error("useWalletContext must be used within a WalletProvider");
  }
  return context;
};